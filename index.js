const express = require('express');
const config = require('@config');
const app = express();

global.__basedir = __dirname;

app.listen(
  config.SERVER_PORT,
  () => console.log(`Listening on port ${config.SERVER_PORT}`)
);

require('./start/utils')(app, express);
require('./start/db')(config);
require('./start/routers')(app);
require('./start/passport')(app);