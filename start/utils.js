const cors = require('cors');
const HttpStatus = require('http-status-codes');
const bodyParser = require('body-parser');

module.exports = (app) => {
  app.use(cors({
    origin: "*",
    optionsSuccessStatus: HttpStatus.OK
  }))
  
  app.use(bodyParser.json());

}