const mongoose = require('mongoose');

module.exports = (config) => {
  return mongoose.connect(config.DB_URL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  })
  .then(() => console.log(`Successfully connected to MongoDB ${config.DB_URL}`))
  .catch(err => console.error(`Couldn't connect to MongoDB`, err))
}