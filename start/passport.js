const passport = require('passport');
const { Strategy: JWTStrategy, ExtractJwt} = require('passport-jwt')
const User = require('@models/user')
const config = require('@config')

const opts = {}
//extract token from bearer
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken()
opts.secretOrKey = config.JWTSECRET

module.exports = (app) => {
  app.use(passport.initialize());

  passport.use(
    new JWTStrategy(opts, (jwt_payload, done) => {
      User.findById(jwt_payload.userId).select('-password').populate('role').then(user => {
        if(user){
          return done(null, {
            user
          }) 
        }

        return done(null, false)
      }).catch(err => {
        console.log(err.toString())
      })

    })
  )
}