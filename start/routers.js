const routes = require('../api/routers')

module.exports = (app) => {
  app.use('/api', routes)
}