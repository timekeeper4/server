const User = require('@models/user')
const populate = ['role']

const { success, error } = require('@helpers/output')

let Controllers = {
  login: async (req, res) => {
    const {
      body: { username, password }
    } = req

    let user = await User.findByUserNameorEmail(username, populate)

    if (!username) return error(res, 'Username field is required')
    if (!password) return error(res, 'Password field is required')
    if (!user) return error(res, "You have entered an invalid username and/or password")
    if (user.forApproval) return error(res, "User is still for approval")

    if (user.comparePassword(password)) {
      const token = user.generateToken()
      
      return success(res, {
        user,
        token: `Bearer ${token}`
      })
    } else {
      return error(res, "You have entered an invalid username and/or password")
    }

  },
  validate: async (req, res) => {
    let { user } = req.user
    let token = null
    if (user) {
      user = await User.findOne({ 
        _id: user._id, 
        active: true
      })
      .populate(populate)
      
      token = `Bearer ${user.generateToken()}`
    }

    return success(res, { user, token })
  }
}

module.exports = { ...Controllers }