const Yup = require('yup');
const { error, success } = require('@helpers/output');

const validator = {
  validate: (req, res, next) => Yup.object().shape({
  })
  .validate(req.body, { abortEarly: false })
  .then(() => next())
  .catch(err => error(res, err))
}

module.exports = validator