const Model = require('@models/user');
const Role = require('@models/role');

const { error, success } = require('@helpers/output');

const populate = ['role']

const Controllers = {
  create: async (req, res) => {
    let {
      firstName,
      middleName,
      lastName,
      email,
      password,
      username,
      gender
    } = req.body
    let isEmailExists = await Model.findOne({ email, deleted: { $ne: true } })
    let isUsernameExists = await Model.findOne({ username, deleted: { $ne: true } })

    let employeeRole = await Role.findOne({ "accessLevel": 2 })

    switch(true) {
      case isEmailExists != null || isEmailExists != undefined:
        return error(res, 'Email already in used', 409);
      case isUsernameExists != null || isUsernameExists != undefined:
        return error(res, 'Username already in used', 409);
    }

    let createData = {
        firstName,
        middleName,
        lastName,
        email,
        username: username ? username : email,
        password,
        role: employeeRole._id,
        gender
    }
  
    return Model.create({ ...createData })
    .then(result => {
      return success(res, result)
    })
    .catch(err => error(res, err))
  },

  getUser: async (req, res) => {
    const { id } = req.params
  
    return Model
    .findOne({ _id: id, deleted: false })
    .populate(populate)
    .select('-password')
    .then(result => success(res, result))
    .catch(err => error(res, err))
  },

  paginate: async (req, res) => {
    let { search, limit, skip, sort } = req.query;
    let findParams = {}
    let roleParams = {}

    if (search) {
      roleParams['$or'] = [
        { 'name': { $regex: `${search}`, $options: 'i' } }
      ]
      let filteredRole = await Role.find(roleParams).distinct('_id');
      findParams['$or'] = [
        { 'firstName': { $regex: `${search}`, $options: 'i' } },
        { 'lastName': { $regex: `${search}`, $options: 'i' } },
        { 'email': { $regex: `${search}`, $options: 'i' } },
        { 'role': { $in: filteredRole } },
      ]
    }
    
    findParams['forApproval'] = true
    return Model.dataTables({
      find: findParams,
      limit,
      skip,
      populate,
      sort: { createdAt: sort && sort == 'desc' ? -1 : 1 }
    })
    .then(result => success(res, result))
    .catch(err => error(res, err))
  },

  update: async (req, res) => {
    const { id } = req.params
    
    return Model
    .updateOne(
      { _id: id },
      { forApproval: false }
    )
    .then(result => success(res, result))
    .catch(err => error(res, err))
  },

  getUser: async (req, res) => {
    const { id } = req.params
  
    return Model
    .findOne({ _id: id })
    .populate(populate)
    .select('-password')
    .then(result => success(res, result))
    .catch(err => error(res, err))
  },
}

module.exports = { ...Controllers }