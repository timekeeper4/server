const Yup = require('yup');
const { error, success } = require('@helpers/output');

const validator = {
  validate: (req, res, next) => Yup.object().shape({
    firstName: Yup.string().required(),
    middleName: Yup.string(),
    lastName: Yup.string().required(),
    email: Yup.string().required(),
    username: Yup.string().required(),
    password: Yup.string().required()
  })
  .validate(req.body, { abortEarly: false })
  .then(() => next())
  .catch(err => error(res, err))
}

module.exports = validator