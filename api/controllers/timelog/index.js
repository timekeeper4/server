const Model = require('@models/timelog');
let dateUtils = require('@middleware/plugins/date')
let populate = ['user']

const { success, error } = require('@helpers/output');

const actions = {
    create: async (req, res) => {
        let { user } = req.user
        let {timeIn, timeOut} = req.body

        let newtimeIn = dateUtils.formatTimezone(timeIn)
        let newtimeOut = dateUtils.formatTimezone(timeOut)
        
        return Model.create({
            user: user == undefined ? user._id : user,
            timeIn: newtimeIn,
            timeOut: newtimeOut
        })
        .then(result => success(res, result))
        .catch(err => error(res, err))
    },
    update: async (req, res) => {
        let { id } = req.params || ''
        let {timeIn, timeOut} = req.body
        
        let timelog = await Model.findOne({ _id: id, deleted: { $ne: true } })

        let newDate = dateUtils.formatTimezone(timeIn)
        let newtimeOut = dateUtils.formatTimezone(timeOut)

        if (!timelog)
            return error(res, 'No timelog record found', 400)

        return Model.findOneAndUpdate(
            { _id: timelog._id },
            {
                timeIn: newDate,
                timeOut: newtimeOut
            },
            { new: true }
        )
        .then(result => success(res, result))
        .catch(err => error(res, err))
    },
    get: async (req, res) => {
        let { id } = req.params
        
        return Model.findOne({ _id: id, deleted: { $ne: true } })
        .populate(populate)
        .then(result => success(res, result))
        .catch(err => error(res, err))
    },
    paginate: async (req, res) => {
        let { limit, skip } = req.query
        let findParams = {}
        let { user } = req.user

        if(user.role.accessLevel == 2){
            findParams['user'] = user._id
        }
        return Model.dataTables({
            find: findParams,
            limit,
            skip,
            populate: [
                {
                    path: 'user',
                    populate: 'role'
                }
            ]
        })
        .then(result => success(res, result))
        .catch(err => error(res, err))
    }
}

module.exports = actions