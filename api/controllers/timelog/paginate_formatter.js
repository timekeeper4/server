const dateUtils = require('@middleware/plugins/date')

module.exports = {
    paginateResource: (arr) => {
        let val = { ...arr } || {}
        let data = [ ...val.data ] || []
    
        if (data && data.length) {
            data = data.map(val => {
                let newData = { ...val }
                let filteredData = newData._doc ? newData._doc : newData

                if (val.timeIn && val.timeOut) {
                    filteredData.duration = dateUtils.duration(val.timeIn, val.timeOut)
                }

                return filteredData
            })
        }
        
        val.data = data
        return val
    }
}