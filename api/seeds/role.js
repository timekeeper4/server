const Role = require('@models/role');

let seeds = async () => {
  let data = [
    {
      name: 'System Administrator',
      accessLevel: 1,
      // role_type: {
      //   sys_admin: true
      // },
    },
    {
      name: 'Employee',
      accessLevel: 2,
      // role_type: {
      //   va_manager: true
      // }
    },
  ]

  for(let i = 0; i < data.length; i++) {
    if (await Role.findOneAndUpdate({ accessLevel: data[i].accessLevel }, data[i])) continue
    else await Role.create(data[i])
  }
}

module.exports = seeds