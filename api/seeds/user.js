let User = require('@models/user');
let Role = require('@models/role');

let seeds = async () => {
  let admin = await Role.findOne({ "accessLevel": 1 })
  let employee = await Role.findOne({ "accessLevel": 2 })

  let users = [
    {
      firstName: 'Admin',
      middleName: 'Admin',
      lastName: 'Admin',
      username: 'admin',
      email: 'admin@example.com',
      password: 'adminadmin',
      role: admin._id,
      gender: 'Male',
      address: 'test',
      forApproval: false
    },
    {
      firstName: 'Employee',
      middleName: 'Employee',
      lastName: 'Employee',
      username: 'employee',
      email: 'employee@example.com',
      password: 'password',
      role: employee._id,
      gender: 'Male',
      address: 'test',
      forApproval: false
    }
  ]

  for (let data of users) {
    console.log(`${data.username} is created`)
    await User.create(data)
  }

  return users
}

module.exports = seeds