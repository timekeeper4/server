const seeder = require('mongoose-seed')
const config = require('@config')

const modelName = process.argv[2]
const runtype = process.argv[3]

const seedModel = require('./' + modelName + '')

if(modelName != 'all'){
  let data = seedModel(modelName)

  seeder.connect(config.DB_URL, {
    useCreateIndex: true,
    useNewUrlParser: true, 
    useUnifiedTopology: true 
  }, async function() {
    if(data.then){
      await data.then(res=>{
        data = res
      })
    }
    data = [
      {
        'model': modelName,
        'documents': data
      }
    ]

    if(runtype != 'update_only'){
      seeder.loadModels([`@models/${modelName}.js`]);
      if(runtype == 'run_only'){
        seeder.populateModels(data, function() {
          seeder.disconnect();
        });
      }else{
        seeder.clearModels([modelName], function() {
          seeder.populateModels(data, function() {
            seeder.disconnect();
          });
        });
      }
    }else{
      seeder.disconnect();
    }
    
  });

}
