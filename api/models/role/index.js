const mongoose = require('mongoose');
const RoleStatics = require('./statics');
const RoleMethods = require('./methods');
const RolePlugins = require('@middleware/plugins/mongoose')

const Schema = new mongoose.Schema({
  name: {
    type: String,
    requied: true
  },
  // accesslevel = 1 ADMIN || EMPOYEE
  accessLevel: Number,
  deleted: {
    type: Boolean,
    default: false
  }
},
{
  timestamps: true,
  autoCreate: true
});

Schema.index({ name: 1, accessLevel: 1 }, { unique: true })
Schema.statics = { ...RoleStatics };
Schema.methods = { ...RoleMethods };
Schema.plugin(RolePlugins.pagination)

module.exports = mongoose.model('role', Schema);

