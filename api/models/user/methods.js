const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const UserMethods = {
  generateToken: function (options) {
    return jwt.sign(Object.assign({
      userId: this._id
    }, options), process.env.JWT_SECRET, {
      expiresIn: process.env.JWT_EXPIRATION
    })
  },
  comparePassword: function (password) {
    return bcrypt.compareSync(password, this.password)
  },
  compareOldPassword: function (oldPassword) {
    return bcrypt.compareSync(oldPassword, this.oldPassword)
  },
  hashPassword: function (password) {
    return bcrypt.hashSync(password)
  }
}

module.exports = UserMethods