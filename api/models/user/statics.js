const statics = {
  findByUserNameorEmail: async function (username, populate) {

    return await this.findOne({
      deleted: false,
      $or: [
        { username },
        { email: username }
      ]
    })
    .populate(populate)
  },
}

module.exports = statics