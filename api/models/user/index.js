const mongoose = require('mongoose');
const UserStatics = require('./statics');
const UserMethods = require('./methods');
const UserPlugin = require('@middleware/plugins/mongoose');
const bcrypt = require('bcryptjs');

const Schema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true
  },
  middleName: String,
  lastName: {
    type: String,
    required: true
  },
  email: String,
  address: String,
  gender: {
    type: String,
    enum: ['Male', 'Female', 'Others']
  },
  forApproval: {
    type: Boolean,
    default: true
  },
  username: String,
  password: String,

  role: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'role'
  },

  approvedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },

  active: {
    type: Boolean,
    default: true
  },
  deleted: {
    type: Boolean,
    default: false
  },
},
{
  timestamps: true,
  autoCreate: true 
})

Schema.set('toObject', { virtuals: true })
Schema.set('toJSON', {
  virtuals: true,
  transform: function (doc, ret) {
    delete ret['password']
    return ret
  }
})

Schema.virtual('fullName').get(function () {
  return `${this.lastName}, ${this.firstName} ${this.middleName}`
})

Schema.pre('save', function(){
  this.password = bcrypt.hashSync(this.password, 10)
})


Schema.statics = { ...UserStatics };
Schema.methods = { ...UserMethods };
Schema.plugin(UserPlugin.pagination)

module.exports = mongoose.model('user', Schema)