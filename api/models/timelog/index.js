let mongoose = require('mongoose');
let TimelogStatics = require('./statics');
let TimelogMethods = require('./methods');
let TimelogPlugins = require('@middleware/plugins/mongoose');

const Schema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    timeIn: {
        type: Date,
        required: true
    },
    timeOut: {
        type: Date,
        required: true
    },
    deleted: {
        type: Boolean,
        default: false
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    updatedBy:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    }
},
{
    timestamps: true,
    autoCreate: true
});

Schema.statics = { ...TimelogStatics };
Schema.methods = { ...TimelogMethods };
Schema.plugin(TimelogPlugins.pagination);

module.exports = mongoose.model('timelog', Schema)

