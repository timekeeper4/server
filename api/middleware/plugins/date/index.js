const moment = require('moment-timezone')

const dateUtils = {
  timezone:'Asia/Manila',
  dateFormat:'YYYY-MM-DD',
  currentDate(){
    return moment().tz(this.timezone).format(this.dateFormat)
  },
  now(){
    return moment().tz(this.timezone).format()
  },
  format(date, format){
    return moment(date).tz(this.timezone).format(format || this.dateFormat) 
  },
  fromNow(timestamp, now){
    return moment(timestamp).tz(this.timezone).fromNow(true)
  },
  formatTimezone(date){
    return moment(date).tz(this.timezone).format()
  }
}

module.exports = dateUtils
