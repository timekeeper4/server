function pagination (schema, options) {
  options = options || {}
  let totalKey = options.totalKey || 'total'
  let dataKey = options.dataKey || 'data'
  let formatters = options.formatters || {}

  schema.statics.dataTables = function (params, callback) {
    if (typeof params === 'function') {
      callback = params
      params = {}
    }

    callback = callback || function () {}

    let thisSchema = this
    let limit = parseInt(params.limit, 10)
    let skip = parseInt(params.skip, 10)
    let select = params.select || {}
    let find = params.find || {}
    let sort = params.sort || {}
    let search = params.search || {}
    let order = params.order || []
    let columns = params.columns || []
    let formatter = params.formatter

    if (search && search.value && search.fields && search.fields.length) {
      let searchQuery = {
        '$regex': search.value,
        '$options': 'i'
      }

      if (search.fields.length === 1) {
        find[search.fields[0] = searchQuery]
      } else if (search.fields.length > 1) {
        if (!find.$or) {
          find.$or = []
        }
        search.fields.forEach(function (el) {
          let obj = {}
          obj[el] = searchQuery
          find.$or.push(obj)
        })
      }
    }

    if (order.length > 0 && columns.length > 0) {
      let sortByOrder = order.reduce((memo, order) => {
        let column = columns[order.column];
        memo[column.data] = order.dir === 'asc' ? 1 : -1;
        return memo
      })

      if (Object.keys(sortByOrder).length) {
        sort = sortByOrder
      }
    }

    let query = thisSchema
    .find(find)
    .select(select)
    .skip(skip)
    .limit(limit)
    .sort(sort)

    if (params.populate) {
      query.populate(params.populate)
    }

    return new Promise((resolve, reject) => {
      if (formatter
        && !(typeof formatter === 'function')
        || (
          typeof formatter === 'string'
          && formatter[formatter]
        )  
      ) {
        return reject(new Error('Invalid formatter'))
      }

      Promise
      .all([query.exec(), thisSchema.countDocuments(find)])
      .then(result => {
        let response = {}
        response[totalKey] = result[1]

        if (
          typeof formatter === 'string'
          && formatters[formatter]
        ) {
          response[dataKey] = result[0].map(formatters[formatter])
        } else if (typeof formatter === 'function') {
          response[dataKey] = result[0].map(formatter)
        } else {
          response[dataKey] = result[0]
        }

        resolve(response)
        callback(null, response)
      })
        .catch(err => {
          reject(err)
          callback(err)
        })
    })
  }
}

module.exports = pagination