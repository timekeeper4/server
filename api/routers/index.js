const { Router } = require('express');
const router = new Router();

router.use('/auth', require('./auth'));
router.use('/user', require('./user'));
router.use('/timelog', require('./timelog'));

module.exports = router