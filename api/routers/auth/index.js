const { Router } = require('express');
const Controllers = require('@controllers/auth');
const { validate } = require('@controllers/auth/validator');
const { verifyToken } = require('@helpers/verify')

const routes = new Router()

routes.get('/validate', verifyToken, Controllers.validate)
routes.post('/login', validate, Controllers.login)

module.exports = routes