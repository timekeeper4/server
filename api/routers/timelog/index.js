const { Router } = require('express');
const routes = new Router();
const Controllers = require('@controllers/timelog');
const { verifyToken } = require('@helpers/verify');

routes.get('/paginate', verifyToken,  Controllers.paginate);
routes.get('/:id', verifyToken, Controllers.get);

routes.post('/', verifyToken, Controllers.create);

routes.put('/:id', verifyToken, Controllers.update);

module.exports = routes;
