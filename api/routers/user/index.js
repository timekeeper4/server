const { Router } = require('express');
const routes = new Router();
const Controllers = require('@controllers/user');
const { verifyToken } = require('@helpers/verify');
const { validate } = require('@controllers/user/validator');

routes.get('/paginate', verifyToken,  Controllers.paginate);
routes.get('/:id', verifyToken, Controllers.getUser);

routes.post('/', validate, Controllers.create);

routes.put('/:id', verifyToken, Controllers.update);

module.exports = routes;
