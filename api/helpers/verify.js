const passport = require('passport');
const jwt = require('jsonwebtoken');

const auth = {
  verifyToken: passport.authenticate('jwt', { session: false })
}

module.exports = auth