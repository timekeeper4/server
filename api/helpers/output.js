const HttpStatus = require('http-status-codes');

const output = {
  error : (res, err, customStatus) => {
    if (!res.headersSent) {
      return res
      .status(customStatus || HttpStatus.BAD_REQUEST)
      .send(typeof err === 'string' ? { message : err, success: false } : err)
    }
  },
  success: (res, payload, attachment) => {
    if (!res.headerSent && !attachment) {
      return res.status(HttpStatus.OK).json(payload)
    }

    if (
      !res.headerSent
      && typeof attachment == 'string' 
      && attachment.toLowerCase() == 'attachment'
      && typeof payload == 'object'
    ) {
      return res.status(HttpStatus.OK).sendFile(payload.path, payload.root)
    }
  }
}

module.exports = output