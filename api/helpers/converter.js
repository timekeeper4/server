const fs = require('fs');
const csvjson = require('csvjson');

let converters = {
  pdfConverter: (data, filePath, fileName) => {
    const csvData = csvjson.toCSV(data, {
      headers: 'key'
    })
  
    fs.writeFile(filePath, csvData, err => {
      if (err) return console.log("FATAL ERROR: ", err)
    })
  
    let result = {
      url: `${process.env.SERVER_URL}/uploads/csv/${fileName}`,
      fileName,
    }
    return result
  }
}

module.exports = converters