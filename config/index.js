const dotenv = require('dotenv');
const path = require('path');

dotenv.config({ path: path.join(__dirname, '../.env') })

module.exports = {
  DB_URL: `${process.env.DB_HOST}`,

  JWTSECRET: process.env.JWT_SECRET,
  JWTEXPIRATION: process.env.JWT_EXPIRATION,

  SERVER_PORT: process.env.SERVER_PORT
}